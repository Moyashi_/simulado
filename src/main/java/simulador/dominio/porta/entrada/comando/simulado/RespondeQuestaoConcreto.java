package simulador.dominio.porta.entrada.comando.simulado;

import simulador.dominio.modelo.simulado.prova.Prova;
import simulador.dominio.porta.saida.ProvaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RespondeQuestaoConcreto implements RespondeQuestao {

    private final ProvaRepository provaRepository;

    @Autowired
    public RespondeQuestaoConcreto(ProvaRepository provaRepository) {
        this.provaRepository = provaRepository;
    }

    @Override
    public void executar(ResponderQuestao responderQuestao) {
        Prova prova = provaRepository.consultarProva(responderQuestao.getProvaId());
        prova.responderQuestao(responderQuestao.getQuestaoId(), responderQuestao.getResposta());
        provaRepository.atualizarProva(prova);
    }
}
