package simulador.dominio.porta.entrada.comando.simulado;

public interface RespondeQuestao {

    void executar(ResponderQuestao responderQuestao);
}
