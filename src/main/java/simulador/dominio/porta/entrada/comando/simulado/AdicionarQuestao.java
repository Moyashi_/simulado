package simulador.dominio.porta.entrada.comando.simulado;

import simulador.dominio.modelo.simulado.questaodaprova.PesoDaQuestao;
import simulador.dominio.modelo.simulado.questaodaprova.RespostaDaQuestao;

public class AdicionarQuestao {

    private final String pergunta;
    private final RespostaDaQuestao respostaDaQuestao;
    private final PesoDaQuestao peso;

    public AdicionarQuestao(String pergunta, RespostaDaQuestao respostaDaQuestao, PesoDaQuestao peso) {
        this.pergunta = pergunta;
        this.respostaDaQuestao = respostaDaQuestao;
        this.peso = peso;
    }

    public String getPergunta() {
        return pergunta;
    }

    public RespostaDaQuestao getRespostaDaQuestao() {
        return respostaDaQuestao;
    }

    public PesoDaQuestao getPeso() {
        return peso;
    }
}
