package simulador.dominio.porta.entrada.comando.simulado;

import simulador.dominio.modelo.simulado.questaodaprova.Questao;

public interface AdicionaQuestao {
    Questao adicionaQuestao(AdicionarQuestao questao);
}
