package simulador.dominio.porta.entrada.comando.simulado;

import simulador.dominio.porta.entrada.comando.Comando;

import java.util.Objects;

public class ResponderQuestao extends Comando {

    private final String provaId;
    private final String questaoId;
    private final String resposta;

    public ResponderQuestao(String provaId, String questaoId, String resposta) throws Exception {
        validarCampoObrigatorio(provaId, questaoId, resposta);
        this.provaId = provaId;
        this.questaoId = questaoId;
        this.resposta = resposta;
    }

    private void validarCampoObrigatorio(String provaId, String questaoId, String resposta) throws Exception {
        if (Objects.isNull(provaId)) {
            throw new Exception("É preciso informar a provaId.");
        }
        if (Objects.isNull(questaoId)) {
            throw new Exception("É preciso informar a questaoId.");
        }
        if (Objects.isNull(resposta)) {
            throw new Exception("É preciso informar a resposta.");
        }
    }

    public String getProvaId() {
        return provaId;
    }

    public String getQuestaoId() {
        return questaoId;
    }

    public String getResposta() {
        return resposta;
    }
}
