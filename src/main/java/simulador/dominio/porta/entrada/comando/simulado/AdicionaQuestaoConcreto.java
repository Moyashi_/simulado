package simulador.dominio.porta.entrada.comando.simulado;

import simulador.dominio.modelo.simulado.questaodaprova.Questao;
import simulador.dominio.porta.saida.QuestaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdicionaQuestaoConcreto implements AdicionaQuestao {

    private final QuestaoRepository questaoRepository;

    @Autowired
    public AdicionaQuestaoConcreto(QuestaoRepository questaoRepository) {
        this.questaoRepository = questaoRepository;
    }

    @Override
    public Questao adicionaQuestao(AdicionarQuestao comando) {
        Questao questao = new Questao(comando.getPergunta(), comando.getRespostaDaQuestao(), comando.getPeso());
        return questaoRepository.save(questao);
    }
}
