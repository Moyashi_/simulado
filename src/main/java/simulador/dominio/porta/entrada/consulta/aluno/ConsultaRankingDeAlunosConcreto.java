package simulador.dominio.porta.entrada.consulta.aluno;

import simulador.dominio.modelo.perfil.aluno.Aluno;
import simulador.dominio.porta.saida.AlunoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ConsultaRankingDeAlunosConcreto implements ConsultaRankingDeAlunos {

    private final AlunoRepository alunoRepository;

    @Autowired
    public ConsultaRankingDeAlunosConcreto(AlunoRepository alunoRepository) {
        this.alunoRepository = alunoRepository;
    }

    @Override
    public List<AlunoDTO> executar() {
        List<Aluno> ranking = alunoRepository.consultarRanking();
        List<AlunoDTO> alunosDTO = new ArrayList<>();
        ranking.forEach(aluno -> {
            AlunoDTO alunoDTO = new AlunoDTO();
            alunoDTO.historicoDeNotas = aluno.historicoDeNotas();
            alunosDTO.add(alunoDTO);
        });
        return alunosDTO;
    }
}