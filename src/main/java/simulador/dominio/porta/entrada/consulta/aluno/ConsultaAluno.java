package simulador.dominio.porta.entrada.consulta.aluno;

import simulador.dominio.modelo.perfil.aluno.Aluno;

public interface ConsultaAluno {
    Aluno consultar(Integer alunoId);
}
