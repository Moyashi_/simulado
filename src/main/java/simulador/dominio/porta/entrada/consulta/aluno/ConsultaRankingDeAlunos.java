package simulador.dominio.porta.entrada.consulta.aluno;

import java.util.List;

public interface ConsultaRankingDeAlunos {
    List<AlunoDTO> executar();
}
