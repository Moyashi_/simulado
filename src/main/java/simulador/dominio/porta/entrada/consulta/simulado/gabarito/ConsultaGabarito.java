package simulador.dominio.porta.entrada.consulta.simulado.gabarito;

import simulador.dominio.porta.entrada.consulta.simulado.RespostaDaQuestaoDTO;

import java.util.List;

public interface ConsultaGabarito {
    List<RespostaDaQuestaoDTO> consultarGabaritoDaProva(ConsultarGabarito consultarGabarito);
}
