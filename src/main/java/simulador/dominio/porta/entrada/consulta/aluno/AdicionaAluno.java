package simulador.dominio.porta.entrada.consulta.aluno;

public interface AdicionaAluno {

    void adicionar(String nome, String cpf, String dataDeNascimento, String nivelDeAcesso);
}
