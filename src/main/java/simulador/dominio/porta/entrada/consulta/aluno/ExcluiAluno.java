package simulador.dominio.porta.entrada.consulta.aluno;

public interface ExcluiAluno {

    void excluir(String alunoId);
}
