package simulador.dominio.porta.entrada.consulta.aluno;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import simulador.dominio.modelo.perfil.NivelDeAcesso;
import simulador.dominio.modelo.perfil.aluno.Aluno;
import simulador.dominio.porta.saida.AlunoRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
public class AdicionaAlunoConcreto implements AdicionaAluno {

    private final AlunoRepository alunoRepository;

    @Autowired
    public AdicionaAlunoConcreto(AlunoRepository alunoRepository) {
        this.alunoRepository = alunoRepository;
    }

    @Override
    public void adicionar(String nome,
                          String cpf,
                          String dataDeNascimentoRecebida,
                          String nivelDeAcessoRecebido) {
        LocalDate dataDeNascimento = formatarDataDeNascimento(dataDeNascimentoRecebida);
        NivelDeAcesso nivelDeAcesso = NivelDeAcesso.valueOf(nivelDeAcessoRecebido);

        Aluno alunoNovo = new Aluno(nome, cpf, dataDeNascimento, nivelDeAcesso);

        alunoRepository.insert(alunoNovo);
    }

    private LocalDate formatarDataDeNascimento(String dataDeNascimento) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return LocalDate.parse(dataDeNascimento, formatter);
    }
}
