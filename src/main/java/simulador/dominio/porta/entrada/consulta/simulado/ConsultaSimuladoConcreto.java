package simulador.dominio.porta.entrada.consulta.simulado;

import simulador.dominio.modelo.simulado.prova.Prova;
import simulador.dominio.modelo.simulado.Simulado;
import simulador.dominio.porta.saida.SimuladoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ConsultaSimuladoConcreto implements ConsultaSimulado {

    private final SimuladoRepository simuladoRepository;

    @Autowired
    public ConsultaSimuladoConcreto(SimuladoRepository simuladoRepository) {
        this.simuladoRepository = simuladoRepository;
    }

    @Override
    public List<SimuladoDTO> executar() {
        List<Simulado> simulados = simuladoRepository.findAll();

        List<SimuladoDTO> simuladosDTO = new ArrayList<>();
        simulados.forEach(simulado -> {
            SimuladoDTO simuladoDTO = montarSimuladoDTO(simulado);
            simuladosDTO.add(simuladoDTO);
        });
        return simuladosDTO;
    }

    private SimuladoDTO montarSimuladoDTO(Simulado simulado) {
        SimuladoDTO simuladoDTO = new SimuladoDTO();
        simuladoDTO.provas = montarProvaDTO(simulado);
        return simuladoDTO;
    }

    private List<ProvaDTO> montarProvaDTO(Simulado simulado) {
        List<ProvaDTO> provasDTO = new ArrayList<>();
        simulado.getProvas().forEach(prova -> {
            ProvaDTO provaDTO = new ProvaDTO();
            provaDTO.notaFinal = prova.getNotaFinal();
            provaDTO.questoes = montarQuestaoDTO(prova);
            provaDTO.gabarito = montarGabaritoDTO(prova);
            provasDTO.add(provaDTO);
        });
        return provasDTO;
    }

    private List<QuestaoDaProvaDTO> montarQuestaoDTO(Prova prova) {
        List<QuestaoDaProvaDTO> questoesDaProvaDTO = new ArrayList<>();

        prova.getQuestoes().forEach(questaoDaProva -> {
            QuestaoDaProvaDTO questaoDaProvaDTO = new QuestaoDaProvaDTO();
            questaoDaProvaDTO.pesoDaQuestao = questaoDaProva.getPesoDaQuestao();
            questaoDaProvaDTO.pergunta = questaoDaProva.getPergunta();

            RespostaDaQuestaoDTO respostaDaQuestaoDTO = new RespostaDaQuestaoDTO();
            respostaDaQuestaoDTO.resposta = questaoDaProva.getRespostaDaQuestao().getDescricao();
            questaoDaProvaDTO.respostaDaQuestao = respostaDaQuestaoDTO;

            RespostaDaQuestaoDTO respostaMarcadaDTO = new RespostaDaQuestaoDTO();
            respostaMarcadaDTO.resposta = questaoDaProva.getRespostaMarcada().getDescricao();
            questaoDaProvaDTO.respostaMarcada = respostaMarcadaDTO;

            questoesDaProvaDTO.add(questaoDaProvaDTO);
        });
        return questoesDaProvaDTO;
    }

    private List<RespostaDaQuestaoDTO> montarGabaritoDTO(Prova prova) {
        List<RespostaDaQuestaoDTO> gabaritoDTO = new ArrayList<>();
        prova.getGabarito().forEach(respostaDaQuestao -> {
            RespostaDaQuestaoDTO respostaDaQuestaoDTO = new RespostaDaQuestaoDTO();
            respostaDaQuestaoDTO.resposta = respostaDaQuestao.getDescricao();
            gabaritoDTO.add(respostaDaQuestaoDTO);
        });
        return gabaritoDTO;
    }
}
