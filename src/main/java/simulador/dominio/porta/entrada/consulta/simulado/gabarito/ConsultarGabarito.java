package simulador.dominio.porta.entrada.consulta.simulado.gabarito;

import java.util.Objects;

public class ConsultarGabarito {

    private final String provaId;

    public ConsultarGabarito(String provaId) throws Exception {
        validarCampoObrigatorio(provaId);
        this.provaId = provaId;
    }

    private void validarCampoObrigatorio(String provaId) throws Exception {
        if(Objects.isNull(provaId)){
            throw new Exception("É preciso informar a provaId.");
        }
    }

    public String getProvaId() {
        return provaId;
    }
}
