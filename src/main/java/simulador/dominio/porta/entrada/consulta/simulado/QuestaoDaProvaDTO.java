package simulador.dominio.porta.entrada.consulta.simulado;

import simulador.dominio.modelo.simulado.questaodaprova.PesoDaQuestao;

public class QuestaoDaProvaDTO {

    public String pergunta;
    public RespostaDaQuestaoDTO respostaDaQuestao;
    public RespostaDaQuestaoDTO respostaMarcada;
    public PesoDaQuestao pesoDaQuestao;
}
