package simulador.dominio.porta.entrada.consulta.aluno;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import simulador.dominio.modelo.perfil.aluno.Aluno;
import simulador.dominio.porta.saida.AlunoRepository;

import java.util.Optional;

@Service
public class ConsultaAlunoConcreto implements ConsultaAluno {

    private final AlunoRepository alunoRepository;

    @Autowired
    public ConsultaAlunoConcreto(AlunoRepository alunoRepository) {
        this.alunoRepository = alunoRepository;
    }

    @Override
    public Aluno consultar(Integer alunoId) {
        Optional<Aluno> aluno = alunoRepository.findById(alunoId);
        return aluno.orElse(null);
    }
}
