package simulador.dominio.porta.entrada.consulta.simulado;

import java.util.List;

public class ProvaDTO {
    public List<QuestaoDaProvaDTO> questoes;
    public List<RespostaDaQuestaoDTO> gabarito;
    public Integer notaFinal;
}
