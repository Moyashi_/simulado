package simulador.dominio.porta.entrada.consulta.simulado;

import java.util.List;

public interface ConsultaSimulado {

    List<SimuladoDTO> executar();
}
