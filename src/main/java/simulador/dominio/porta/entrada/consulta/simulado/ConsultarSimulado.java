package simulador.dominio.porta.entrada.consulta.simulado;

import java.util.Objects;

public class ConsultarSimulado {

    private final Integer simuladoId;

    public ConsultarSimulado(Integer simuladoId) throws Exception {
        validarCampoObrigatorio(simuladoId);
        this.simuladoId = simuladoId;
    }

    private void validarCampoObrigatorio(Integer simuladoId) throws Exception {
        if(Objects.isNull(simuladoId)){
            throw new Exception("É preciso informar o simuladoId.");
        }
    }

    public Integer getSimuladoId() {
        return simuladoId;
    }
}
