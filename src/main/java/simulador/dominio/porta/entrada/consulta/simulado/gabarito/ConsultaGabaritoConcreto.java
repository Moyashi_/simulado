package simulador.dominio.porta.entrada.consulta.simulado.gabarito;

import simulador.dominio.porta.entrada.consulta.simulado.RespostaDaQuestaoDTO;
import simulador.dominio.modelo.simulado.prova.Prova;
import simulador.dominio.porta.saida.ProvaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ConsultaGabaritoConcreto implements ConsultaGabarito {

    private ProvaRepository provaRepository;

    @Override
    public List<RespostaDaQuestaoDTO> consultarGabaritoDaProva(ConsultarGabarito consultarGabarito) {
        Prova prova = provaRepository.consultarProva(consultarGabarito.getProvaId());
        return montarGabaritoDTO(prova);
    }

    private List<RespostaDaQuestaoDTO> montarGabaritoDTO(Prova prova) {
        List<RespostaDaQuestaoDTO> gabaritoDTO = new ArrayList<>();
        prova.getGabarito().forEach(respostaDaQuestao -> {
            RespostaDaQuestaoDTO respostaDaQuestaoDTO = new RespostaDaQuestaoDTO();
            respostaDaQuestaoDTO.resposta = respostaDaQuestao.getDescricao();
            gabaritoDTO.add(respostaDaQuestaoDTO);
        });
        return gabaritoDTO;
    }
}