package simulador.dominio.porta.saida;

import org.springframework.stereotype.Repository;
import simulador.dominio.modelo.perfil.aluno.Aluno;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

@Repository
public interface AlunoRepository extends MongoRepository<Aluno, Integer> {
    List<Aluno> consultarRanking();
}
