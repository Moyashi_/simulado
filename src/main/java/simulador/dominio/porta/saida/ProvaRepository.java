package simulador.dominio.porta.saida;

import simulador.dominio.modelo.simulado.prova.Prova;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProvaRepository extends MongoRepository<Prova,Integer>{

    Prova consultarProva(String provaId);

    void atualizarProva(Prova prova);
}
