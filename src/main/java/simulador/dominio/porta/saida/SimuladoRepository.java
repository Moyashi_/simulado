package simulador.dominio.porta.saida;

import simulador.dominio.modelo.simulado.Simulado;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SimuladoRepository extends MongoRepository<Simulado, Integer> {

    List<Simulado> buscarSimulados();
}
