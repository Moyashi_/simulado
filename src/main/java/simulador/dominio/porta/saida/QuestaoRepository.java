package simulador.dominio.porta.saida;

import simulador.dominio.modelo.simulado.questaodaprova.Questao;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestaoRepository extends MongoRepository<Questao, Integer> {
}
