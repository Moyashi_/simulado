package simulador.dominio.modelo.simulado;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ObjetoDeValor {

    @Id
    private Integer id;

    public ObjetoDeValor() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Id
    public Integer getId() {
        return id;
    }
}
