package simulador.dominio.modelo.simulado;

import simulador.dominio.modelo.Entidade;
import simulador.dominio.modelo.simulado.prova.Prova;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//@Entity
@Document
public class Simulado extends Entidade {

    private List<Prova> provas;


    public Simulado(List<Prova> provas) {
        super();
        this.provas = provas;
    }

    public Simulado() {
        super();
    }

    public List<Prova> getProvas() {
        return provas;
    }

    public int calcularNotaDoSimulado() {
        List<Integer> notaFinalDaProva = new ArrayList<>(Collections.emptyList());
        this.provas.forEach(prova -> notaFinalDaProva.add(obterNotaFinalDaProva(prova)));
        return notaFinalDaProva.stream().reduce(0, Integer::sum);
    }

    private Integer obterNotaFinalDaProva(Prova prova) {
        prova.calcularNotaFinal();
        return prova.getNotaFinal();
    }
}
