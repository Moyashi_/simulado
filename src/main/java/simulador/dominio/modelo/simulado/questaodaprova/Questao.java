package simulador.dominio.modelo.simulado.questaodaprova;

import simulador.dominio.modelo.Entidade;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Column;

@Document
public class Questao extends Entidade {

    @Column
    private String pergunta;

    @Column
    private RespostaDaQuestao respostaDaQuestao;

    @Column
    private RespostaDaQuestao respostaMarcada;

    @Column
    private PesoDaQuestao pesoDaQuestao;

    public Questao(String pergunta,
                   RespostaDaQuestao respostaDaQuestao,
                   PesoDaQuestao pesoDaQuestao) {
        super();
        this.pergunta = pergunta;
        this.respostaDaQuestao = respostaDaQuestao;
        this.pesoDaQuestao = pesoDaQuestao;
    }

    public Questao() {
        super();
    }

    public String getPergunta() {
        return pergunta;
    }

    public RespostaDaQuestao getRespostaDaQuestao() {
        return respostaDaQuestao;
    }

    public RespostaDaQuestao getRespostaMarcada() {
        return respostaMarcada;
    }

    public PesoDaQuestao getPesoDaQuestao() {
        return pesoDaQuestao;
    }

    public int getPeso() {
        return pesoDaQuestao.getPeso();
    }

    public void responderQuestao(RespostaDaQuestao respostaMarcada) {
        this.respostaMarcada = respostaMarcada;
    }

    public boolean acertouAQuestao() {
        return respostaDaQuestao.equals(respostaMarcada);
    }
}