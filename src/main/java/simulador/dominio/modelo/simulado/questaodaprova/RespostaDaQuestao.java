package simulador.dominio.modelo.simulado.questaodaprova;

public enum RespostaDaQuestao {
    A("A"),
    B("B"),
    C("C"),
    D("D"),
    E("E");

    private final String descricao;

    RespostaDaQuestao(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
