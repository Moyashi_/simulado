package simulador.dominio.modelo.simulado.questaodaprova;

public enum PesoDaQuestao {
    FACIL(1),
    MEDIO(2),
    DIFICIL(3);

    private final int peso;

    PesoDaQuestao(int peso) {
        this.peso = peso;
    }

    public int getPeso() {
        return peso;
    }
}
