package simulador.dominio.modelo.simulado.prova;

import simulador.dominio.modelo.Entidade;
import simulador.dominio.modelo.simulado.questaodaprova.PesoDaQuestao;
import simulador.dominio.modelo.simulado.questaodaprova.Questao;
import simulador.dominio.modelo.simulado.questaodaprova.RespostaDaQuestao;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Prova extends Entidade {

    private List<Questao> questoes;

    private Integer notaFinal;

    public Prova(List<Questao> questoes) {
        super();
        validarCamposObrigatorios(questoes);
        validarQuantidadeDeQuestoes(questoes);
        this.questoes = questoes;
        this.notaFinal = 0;
    }

    public Prova() {
        super();
    }

    public List<Questao> getQuestoes() {
        return questoes;
    }

    public List<RespostaDaQuestao> getGabarito() {
        return this.questoes.stream().map(Questao::getRespostaDaQuestao).collect(Collectors.toList());
    }

    public int getNotaFinal() {
        return notaFinal;
    }

    private void validarCamposObrigatorios(List<Questao> questoes) {
        if (listaInvalida(questoes)) {
            throw new IllegalArgumentException("É preciso informar uma lista de questões válida.");
        }
    }

    private boolean listaInvalida(List<?> lista) {
        return lista == null || lista.isEmpty();
    }

    private void validarQuantidadeDeQuestoes(List<Questao> questoes) {
        int questoesFaceis = obterQuantidadeDeQuestoes(questoes, PesoDaQuestao.FACIL);
        int questoesMedias = obterQuantidadeDeQuestoes(questoes, PesoDaQuestao.MEDIO);
        int questoesDificeis = obterQuantidadeDeQuestoes(questoes, PesoDaQuestao.DIFICIL);

        if (!(questoesFaceis == 5) || !(questoesMedias == 5) || !(questoesDificeis == 5)) {
            throw new IllegalArgumentException("Quantidade de questões fora do padrão.");
        }
    }

    private int obterQuantidadeDeQuestoes(List<Questao> questoes, PesoDaQuestao peso) {
        return questoes
                .stream()
                .filter(questaoDaProva -> questaoDaProva.getPesoDaQuestao().equals(peso))
                .toList()
                .size();
    }

    public void calcularNotaFinal() {
        List<Integer> notaFinal = new ArrayList<>(Collections.emptyList());
        this.questoes.forEach(questao -> {
            if (questao.acertouAQuestao()) {
                notaFinal.add(questao.getPeso());
            }
        });
        this.notaFinal = notaFinal.stream().reduce(0, Integer::sum);
    }

    public void responderQuestao(String questaoId, String respostaDaQuestao) {
        Questao questao = questoes.stream()
                .filter(questaoAResponder -> questaoAResponder.getId().equals(questaoId)).
                findFirst()
                .get();
        int posicao = questoes.indexOf(questao);
        questao.responderQuestao(RespostaDaQuestao.valueOf(respostaDaQuestao));
        questoes.set(posicao, questao);
    }
}
