package simulador.dominio.modelo.simulado.prova;

import simulador.dominio.modelo.simulado.ObjetoDeValor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Nota extends ObjetoDeValor
{

    @Column
    private Integer nota;

    public Nota(Integer nota) {
        this.nota = nota;
    }

    public Nota() {

    }

    public Integer getNota() {
        return nota;
    }
}
