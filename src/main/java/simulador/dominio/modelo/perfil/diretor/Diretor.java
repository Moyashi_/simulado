package simulador.dominio.modelo.perfil.diretor;

import simulador.dominio.modelo.perfil.NivelDeAcesso;
import simulador.dominio.modelo.perfil.PerfilBase;

import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
public class Diretor extends PerfilBase {

    public Diretor(String nome,
                   String cpf,
                   LocalDate dataDeNascimento,
                   NivelDeAcesso nivelDeAcesso) {
        super(nome, cpf, dataDeNascimento, nivelDeAcesso);
    }

    public Diretor() {
    }
}
