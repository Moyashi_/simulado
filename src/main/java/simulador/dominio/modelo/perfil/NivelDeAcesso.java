package simulador.dominio.modelo.perfil;

public enum NivelDeAcesso {
    ALUNO(),
    PROFESSOR(),
    DIRETOR(),
    ADMINISTRADOR();

    NivelDeAcesso() {
    }
}
