package simulador.dominio.modelo.perfil.aluno;

import simulador.dominio.modelo.perfil.NivelDeAcesso;
import simulador.dominio.modelo.perfil.PerfilBase;
import simulador.dominio.modelo.simulado.prova.Nota;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Aluno extends PerfilBase {

    @ElementCollection
    private List<Nota> historicoDeNotas;

    public Aluno(String nome,
                 String cpf,
                 LocalDate dataDeNascimento,
                 NivelDeAcesso nivelDeAcesso) {
        super(nome, cpf, dataDeNascimento, nivelDeAcesso);
        this.historicoDeNotas = new ArrayList<>();
    }

    public Aluno() {
        super();
    }

    public List<Nota> getHistoricoDeNotas() {
        return historicoDeNotas;
    }

    public List<Integer> historicoDeNotas() {
        return historicoDeNotas.stream().map(Nota::getNota).collect(Collectors.toList());
    }

    public void adicionarNota(Integer nota) {
        this.historicoDeNotas.add(new Nota(nota));
    }

    public Integer obterMedia() {
        return (this.historicoDeNotas.stream().map(Nota::getNota).reduce(0, Integer::sum))
                / this.historicoDeNotas.size();
    }
}
