package simulador.dominio.modelo.perfil;

import simulador.dominio.modelo.Entidade;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
public class PerfilBase extends Entidade {

    @Column
    private String nome;

    @Column
    private String cpf;

    @Column
    private LocalDate dataDeNascimento;

    @Column
    private NivelDeAcesso nivelDeAcesso;

    public PerfilBase(String nome,
                      String cpf,
                      LocalDate dataDeNascimento,
                      NivelDeAcesso nivelDeAcesso) {
        super();
        this.nome = nome;
        this.cpf = cpf;
        this.dataDeNascimento = dataDeNascimento;
        this.nivelDeAcesso = nivelDeAcesso;
    }

    public PerfilBase() {
        super();
    }

    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public LocalDate getDataDeNascimento() {
        return dataDeNascimento;
    }

    public NivelDeAcesso getNivelDeAcesso() {
        return nivelDeAcesso;
    }
}
