package simulador.dominio.modelo.perfil.professor;

import simulador.dominio.modelo.perfil.NivelDeAcesso;
import simulador.dominio.modelo.perfil.PerfilBase;

import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
public class Professor extends PerfilBase {

    public Professor(String nome,
                     String cpf,
                     LocalDate dataDeNascimento,
                     NivelDeAcesso nivelDeAcesso) {
        super(nome, cpf, dataDeNascimento, nivelDeAcesso);
    }

    public Professor() {
    }
}
