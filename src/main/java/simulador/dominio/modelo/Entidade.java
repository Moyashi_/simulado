package simulador.dominio.modelo;

import javax.persistence.*;
import java.util.UUID;

@MappedSuperclass
public class Entidade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private String id;

    public Entidade() {
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }
}
