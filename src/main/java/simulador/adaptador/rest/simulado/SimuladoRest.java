package simulador.adaptador.rest.simulado;

import simulador.dominio.porta.entrada.consulta.simulado.ConsultaSimulado;
import simulador.dominio.porta.entrada.consulta.simulado.SimuladoDTO;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("simulado")
public class SimuladoRest {

    private final ConsultaSimulado consultaSimulados;

    @Autowired
    public SimuladoRest(ConsultaSimulado consultaSimulados) {
        this.consultaSimulados = consultaSimulados;
    }

    @GET
    public Response obterTodosSimulados() {
        List<SimuladoDTO> simulados = consultaSimulados.executar();
        return Response.ok().entity(simulados).build();
    }
}
