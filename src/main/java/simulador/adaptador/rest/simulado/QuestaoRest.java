package simulador.adaptador.rest.simulado;

import simulador.dominio.porta.entrada.comando.simulado.AdicionaQuestao;
import simulador.dominio.porta.entrada.comando.simulado.AdicionarQuestao;
import simulador.dominio.modelo.simulado.questaodaprova.PesoDaQuestao;
import simulador.dominio.modelo.simulado.questaodaprova.Questao;
import simulador.dominio.modelo.simulado.questaodaprova.RespostaDaQuestao;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("questao")
public class QuestaoRest {

    private final AdicionaQuestao adicionaQuestao;

    @Autowired
    public QuestaoRest(AdicionaQuestao adicionaQuestao) {
        this.adicionaQuestao = adicionaQuestao;
    }

    @GET
    public Response consultarUmaQuestao() {
        return null;
    }

    @POST
    public Response adicionarUmaQuestao(QuestaoHttpDto questaoHttpDto) {
        AdicionarQuestao adicionarQuestao = new AdicionarQuestao(questaoHttpDto.pergunta,
                RespostaDaQuestao.valueOf(questaoHttpDto.respostaDaQuestao), PesoDaQuestao.valueOf(questaoHttpDto.peso));
        Questao questaoCriada = adicionaQuestao.adicionaQuestao(adicionarQuestao);
        return Response.ok().entity(questaoCriada).build();
    }

    @DELETE
    public Response deletarUmaQuestao() {
        return null;
    }
}
