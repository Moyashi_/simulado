package simulador.adaptador.rest.simulado;

import simulador.dominio.porta.entrada.comando.simulado.RespondeQuestao;
import simulador.dominio.porta.entrada.comando.simulado.ResponderQuestao;
import simulador.dominio.porta.entrada.consulta.simulado.gabarito.ConsultaGabarito;
import simulador.dominio.porta.entrada.consulta.simulado.gabarito.ConsultarGabarito;
import simulador.dominio.porta.entrada.consulta.simulado.RespostaDaQuestaoDTO;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("prova")
public class ProvaRest {

    private final ConsultaGabarito consultaGabarito;
    private final RespondeQuestao respondeQuestao;

    @Autowired
    public ProvaRest(ConsultaGabarito consultaGabarito, RespondeQuestao respondeQuestao) {
        this.consultaGabarito = consultaGabarito;
        this.respondeQuestao = respondeQuestao;
    }

    @GET
    @Path("/gabarito/{id}")
    public Response obterGabarito(@PathParam("id") String provaId) throws Exception {
        ConsultarGabarito consultarGabarito = new ConsultarGabarito(provaId);
        List<RespostaDaQuestaoDTO> gabaritoDaProva = consultaGabarito.consultarGabaritoDaProva(consultarGabarito);
        return Response.ok().entity(gabaritoDaProva).build();
    }

    @PUT
    @Path("{provaId}/{questaoId}")
    public Response responderQuestao(@PathParam("provaId") String provaId,
                                     @PathParam("questaoId") String questaoId,
                                     @QueryParam("resposta") String resposta) throws Exception {
        ResponderQuestao responderQuestao = new ResponderQuestao(provaId, questaoId, resposta);
        respondeQuestao.executar(responderQuestao);
        return Response.ok().build();
    }
}
