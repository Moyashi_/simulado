package simulador.adaptador.rest.perfil;

import org.springframework.beans.factory.annotation.Autowired;
import simulador.dominio.modelo.perfil.aluno.Aluno;
import simulador.dominio.porta.entrada.consulta.aluno.*;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("aluno")
public class AlunoRest {

    private final ConsultaRankingDeAlunos consultaRankingDeAlunos;
    private final ConsultaAluno consultaAluno;
    private final AdicionaAluno adicionaAluno;
    private final ExcluiAluno excluiAluno;

    @Autowired
    public AlunoRest(ConsultaRankingDeAlunos consultaRankingDeAlunos,
                     ConsultaAluno consultaAluno,
                     AdicionaAluno adicionaAluno,
                     ExcluiAluno excluiAluno) {
        this.consultaRankingDeAlunos = consultaRankingDeAlunos;
        this.consultaAluno = consultaAluno;
        this.adicionaAluno = adicionaAluno;
        this.excluiAluno = excluiAluno;
    }

    @GET
    @Path("ranking")
    public Response rankingDosAlunos() {
        List<AlunoDTO> ranking = consultaRankingDeAlunos.executar();
        return Response.ok().entity(ranking).build();
    }

    @GET
    @Path("{alunoId}")
    public Response obterAluno(@PathParam("alunoId") Integer alunoId) {
        Aluno alunoConsultado = consultaAluno.consultar(alunoId);
        return Response.ok().entity(alunoConsultado).build();
    }

    @POST
    public Response adicionarAluno(@QueryParam("nome") String nome,
                                   @QueryParam("cpf") String cpf,
                                   @QueryParam("dataDeNascimento") String dataDeNascimento,
                                   @QueryParam("nivelDeAcesso") String nivelDeAcesso) {
        adicionaAluno.adicionar(nome, cpf, dataDeNascimento, nivelDeAcesso);
        return Response.ok().build();
    }

    @DELETE
    @Path("{alunoId}")
    public Response excluirAluno(@PathParam("alunoId") String alunoId) {
        excluiAluno.excluir(alunoId);
        return Response.ok().build();
    }
}
