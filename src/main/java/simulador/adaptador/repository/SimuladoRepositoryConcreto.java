package simulador.adaptador.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import org.springframework.stereotype.Repository;
import simulador.dominio.modelo.simulado.Simulado;
import simulador.dominio.porta.saida.SimuladoRepository;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

//@org.springframework.stereotype.Repository("SimuladoRepository")
@Repository
public class SimuladoRepositoryConcreto implements SimuladoRepository {

    @Autowired
    public SimuladoRepositoryConcreto() {
    }

    @Override
    public List<Simulado> buscarSimulados() {
        return findAll();
    }

    @Override
    public <S extends Simulado> S save(S entity) {
        return null;
    }

    @Override
    public <S extends Simulado> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<Simulado> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public List<Simulado> findAll() {
        return null;
    }

    @Override
    public Iterable<Simulado> findAllById(Iterable<Integer> integers) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public void delete(Simulado entity) {

    }

    @Override
    public void deleteAllById(Iterable<? extends Integer> integers) {

    }

    @Override
    public void deleteAll(Iterable<? extends Simulado> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public List<Simulado> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Simulado> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Simulado> S insert(S entity) {
        return null;
    }

    @Override
    public <S extends Simulado> List<S> insert(Iterable<S> entities) {
        return null;
    }

    @Override
    public <S extends Simulado> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Simulado> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Simulado> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Simulado> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Simulado> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Simulado> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public <S extends Simulado, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
        return null;
    }
}

