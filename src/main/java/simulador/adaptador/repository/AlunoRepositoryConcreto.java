package simulador.adaptador.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import org.springframework.stereotype.Repository;
import simulador.dominio.modelo.perfil.aluno.Aluno;
import simulador.dominio.porta.saida.AlunoRepository;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

//@org.springframework.stereotype.Repository("AlunoRepository")
@Repository
public class AlunoRepositoryConcreto implements AlunoRepository {

    @Autowired
    public AlunoRepositoryConcreto() {
    }

    @Override
    public List<Aluno> consultarRanking() {
        return null;
    }

    @Override
    public <S extends Aluno> S save(S entity) {
        return null;
    }

    @Override
    public <S extends Aluno> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<Aluno> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public List<Aluno> findAll() {
        return null;
    }

    @Override
    public Iterable<Aluno> findAllById(Iterable<Integer> integers) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public void delete(Aluno entity) {

    }

    @Override
    public void deleteAllById(Iterable<? extends Integer> integers) {

    }

    @Override
    public void deleteAll(Iterable<? extends Aluno> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public List<Aluno> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Aluno> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Aluno> S insert(S entity) {
        return null;
    }

    @Override
    public <S extends Aluno> List<S> insert(Iterable<S> entities) {
        return null;
    }

    @Override
    public <S extends Aluno> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Aluno> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Aluno> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Aluno> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Aluno> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Aluno> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public <S extends Aluno, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
        return null;
    }
}
