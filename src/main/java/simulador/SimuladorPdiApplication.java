package simulador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimuladorPdiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimuladorPdiApplication.class, args);
    }

}
