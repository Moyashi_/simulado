package dominio.modelo;

import simulador.dominio.modelo.simulado.prova.Nota;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class NotaTest {

    @Test
    void deveCriarUmaNota() {
        Integer notaEsperada = 8;

        Nota notaCriada = new Nota(notaEsperada);

        Assertions.assertThat(notaCriada.getNota()).isEqualTo(notaEsperada);
    }
}