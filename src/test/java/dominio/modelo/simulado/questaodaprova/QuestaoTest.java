package dominio.modelo.simulado.questaodaprova;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import dominio.builder.QuestaoBuilder;
import simulador.dominio.modelo.simulado.questaodaprova.PesoDaQuestao;
import simulador.dominio.modelo.simulado.questaodaprova.Questao;
import simulador.dominio.modelo.simulado.questaodaprova.RespostaDaQuestao;


class QuestaoTest {

    @Test
    void deveCriarUmaQuestaoDaProva() {
        String perguntaEsperada = "qual a capital do brasil?";
        RespostaDaQuestao respostaDaQuestaoEsperada = RespostaDaQuestao.B;
        PesoDaQuestao pesoDaQuestaoEsperado = PesoDaQuestao.FACIL;

        Questao questao =new Questao(perguntaEsperada, respostaDaQuestaoEsperada, pesoDaQuestaoEsperado);

        Assertions.assertThat(questao.getPergunta()).isEqualTo(perguntaEsperada);
        Assertions.assertThat(questao.getRespostaDaQuestao()).isEqualTo(respostaDaQuestaoEsperada);
        Assertions.assertThat(questao.getPesoDaQuestao()).isEqualTo(pesoDaQuestaoEsperado);
    }

    @Test
    void deveResponderAQuestaoDaProva() {
        RespostaDaQuestao repostaMarcadaEsperada = RespostaDaQuestao.A;
        Questao questao = new QuestaoBuilder().naoRespondida().criar();

        questao.responderQuestao(repostaMarcadaEsperada);

        Assertions.assertThat(questao.getRespostaMarcada()).isEqualTo(repostaMarcadaEsperada);
    }

    @Test
    void deveInformarTrueCasoTenhaAcertadoAQuestao() {
        RespostaDaQuestao repostaMarcada = RespostaDaQuestao.A;
        RespostaDaQuestao respostaCerta = RespostaDaQuestao.A;
        Questao questao = new QuestaoBuilder()
                .comRespostaDaQuestao(respostaCerta)
                .comRespostaMarcada(repostaMarcada)
                .criar();

        boolean acertouAQuestao = questao.acertouAQuestao();

        Assertions.assertThat(acertouAQuestao).isTrue();
    }

    @Test
    void deveInformarFalseAoInformarRespostaDiferenteDaCorreta() {
        RespostaDaQuestao repostaMarcada = RespostaDaQuestao.B;
        RespostaDaQuestao respostaCerta = RespostaDaQuestao.A;
        Questao questao = new QuestaoBuilder()
                .comRespostaDaQuestao(respostaCerta)
                .comRespostaMarcada(repostaMarcada)
                .criar();

        boolean acertouAQuestao = questao.acertouAQuestao();

        Assertions.assertThat(acertouAQuestao).isFalse();
    }
}