package dominio.modelo.simulado;

import dominio.builder.ProvaBuilder;
import simulador.dominio.modelo.simulado.Simulado;
import simulador.dominio.modelo.simulado.prova.Prova;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class SimuladoTest {

    @Test
    void deveCriarUmSimulado() {
        Prova prova = new ProvaBuilder().criar();
        Prova outraProva = new ProvaBuilder().criar();
        List<Prova> provasEsperadas = Arrays.asList(prova, outraProva);

        Simulado simulado = new Simulado(provasEsperadas);

        Assertions.assertThat(simulado.getProvas()).isEqualTo(provasEsperadas);
    }

    @Test
    void deveCalcularNotaDoSimulado() {
        Prova prova = new ProvaBuilder().criar();
        prova.calcularNotaFinal();
        Integer notaFinal = prova.getNotaFinal();
        Prova outraProva = new ProvaBuilder().criar();
        outraProva.calcularNotaFinal();
        Integer notaFinalDaOutraProva = prova.getNotaFinal();
        Integer notaDoSimuladoEsperado = notaFinal + notaFinalDaOutraProva;
        List<Prova> provasDoSimulado = Arrays.asList(prova, outraProva);
        Simulado simulado = new Simulado(provasDoSimulado);

        Integer notaDoSimulado = simulado.calcularNotaDoSimulado();

        Assertions.assertThat(notaDoSimulado).isEqualTo(notaDoSimuladoEsperado);
    }
}