package dominio.modelo.simulado;

import dominio.builder.ProvaBuilder;
import dominio.builder.QuestaoBuilder;
import simulador.dominio.modelo.simulado.prova.Prova;
import simulador.dominio.modelo.simulado.questaodaprova.PesoDaQuestao;
import simulador.dominio.modelo.simulado.questaodaprova.Questao;
import simulador.dominio.modelo.simulado.questaodaprova.RespostaDaQuestao;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.catchThrowable;

class ProvaTest {

    @Test
    void deveCriarUmaProva() {
        Questao questaoFacilDaProva = new QuestaoBuilder().comPesoDaQuestao(PesoDaQuestao.FACIL).criar();
        RespostaDaQuestao respostaDaQuestaoFacil = questaoFacilDaProva.getRespostaDaQuestao();
        Questao questaoMediaDaProva = new QuestaoBuilder().comPesoDaQuestao(PesoDaQuestao.MEDIO).criar();
        RespostaDaQuestao respostaDaQuestaoMedia = questaoMediaDaProva.getRespostaDaQuestao();
        Questao questaoDificilDaProva = new QuestaoBuilder().comPesoDaQuestao(PesoDaQuestao.DIFICIL).criar();
        RespostaDaQuestao respostaDaQuestaoDificil = questaoDificilDaProva.getRespostaDaQuestao();
        List<Questao> questoesDaProva =
                montarQuestoesDaProva(questaoFacilDaProva, questaoMediaDaProva, questaoDificilDaProva);
        List<RespostaDaQuestao> gabarito =
                montarRespostasDaProva(respostaDaQuestaoFacil, respostaDaQuestaoMedia, respostaDaQuestaoDificil);

        Prova prova = new Prova(questoesDaProva);

        Assertions.assertThat(prova.getGabarito()).isEqualTo(gabarito);
        Assertions.assertThat(prova.getQuestoes()).isEqualTo(questoesDaProva);
    }

    @Test
    void DeveObterGabaritoDaProva() {
        Questao questaoFacilDaProva = new QuestaoBuilder().comPesoDaQuestao(PesoDaQuestao.FACIL).criar();
        RespostaDaQuestao respostaDaQuestaoFacil = questaoFacilDaProva.getRespostaDaQuestao();
        Questao questaoMediaDaProva = new QuestaoBuilder().comPesoDaQuestao(PesoDaQuestao.MEDIO).criar();
        RespostaDaQuestao respostaDaQuestaoMedia = questaoMediaDaProva.getRespostaDaQuestao();
        Questao questaoDificilDaProva = new QuestaoBuilder().comPesoDaQuestao(PesoDaQuestao.DIFICIL).criar();
        RespostaDaQuestao respostaDaQuestaoDificil = questaoDificilDaProva.getRespostaDaQuestao();
        List<Questao> questoesDaProva =
                montarQuestoesDaProva(questaoFacilDaProva, questaoMediaDaProva, questaoDificilDaProva);
        List<RespostaDaQuestao> gabaritoEsperado =
                montarRespostasDaProva(respostaDaQuestaoFacil, respostaDaQuestaoMedia, respostaDaQuestaoDificil);
        Prova prova = new Prova(questoesDaProva);

        List<RespostaDaQuestao> gabarito = prova.getGabarito();

        Assertions.assertThat(gabarito).isEqualTo(gabaritoEsperado);
    }

    @Test
    void deveCalcularNotaFinalDaProva() {
        int notaFinalMaximaEsperada = 30;
        Prova provaGabaritada = new ProvaBuilder().criar();

        provaGabaritada.calcularNotaFinal();

        Assertions.assertThat(provaGabaritada.getNotaFinal()).isEqualTo(notaFinalMaximaEsperada);
    }

    @Test
    void deveLancarExcecaoCasoInformeQuantidadeDeQuestoesIncorretas() {
        Questao questao = new QuestaoBuilder().criar();
        List<Questao> questoesComQuantidadeIncorreta = Collections.singletonList(questao);
        Prova prova = new ProvaBuilder().criar();
        String mensagemEsperada = "Quantidade de questões fora do padrão.";

        Throwable excecaoLancada = catchThrowable(() -> new Prova(questoesComQuantidadeIncorreta));

        Assertions.assertThat(excecaoLancada).isInstanceOf(IllegalArgumentException.class).hasMessageContaining(mensagemEsperada);
    }

    @ParameterizedTest
    @MethodSource("obterDadosInvalidosParaCriacao")
    void naoDeveCriarUmaProvaComDadosInvalidos(List<Questao> questoesDaProva,
                                               String mensagemEsperada) {

        Throwable excecaoLancada = catchThrowable(() -> new Prova(questoesDaProva));

        Assertions.assertThat(excecaoLancada).isInstanceOf(IllegalArgumentException.class).hasMessageContaining(mensagemEsperada);
    }

    private List<Questao> montarQuestoesDaProva(Questao questaoFacilDaProva,
                                                Questao questaoMediaDaProva,
                                                Questao questaoDificilDaProva) {
        return Arrays.asList(questaoFacilDaProva, questaoFacilDaProva, questaoFacilDaProva, questaoFacilDaProva,
                questaoFacilDaProva, questaoMediaDaProva, questaoMediaDaProva, questaoMediaDaProva, questaoMediaDaProva,
                questaoMediaDaProva, questaoDificilDaProva, questaoDificilDaProva, questaoDificilDaProva,
                questaoDificilDaProva, questaoDificilDaProva);
    }

    private List<RespostaDaQuestao> montarRespostasDaProva(RespostaDaQuestao respostaDaQuestaoFacil,
                                                           RespostaDaQuestao respostaDaQuestaoMedia,
                                                           RespostaDaQuestao respostaDaQuestaoDificil) {
        return Arrays.asList(respostaDaQuestaoFacil, respostaDaQuestaoFacil, respostaDaQuestaoFacil,
                respostaDaQuestaoFacil, respostaDaQuestaoFacil, respostaDaQuestaoMedia, respostaDaQuestaoMedia,
                respostaDaQuestaoMedia, respostaDaQuestaoMedia, respostaDaQuestaoMedia, respostaDaQuestaoDificil,
                respostaDaQuestaoDificil, respostaDaQuestaoDificil, respostaDaQuestaoDificil, respostaDaQuestaoDificil);
    }

    private static Stream<Arguments> obterDadosInvalidosParaCriacao() {
        return Stream.of(
                Arguments.arguments(null, "É preciso informar uma lista de questões válida."),
                Arguments.arguments(Collections.emptyList(), "É preciso informar uma lista de questões válida.")
        );
    }
}