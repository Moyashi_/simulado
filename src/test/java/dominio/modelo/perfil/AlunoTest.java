package dominio.modelo.perfil;

import simulador.dominio.modelo.perfil.NivelDeAcesso;
import simulador.dominio.modelo.simulado.prova.Nota;
import simulador.dominio.modelo.perfil.aluno.Aluno;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

class AlunoTest {

    @Test
    void deveCriarUmAluno() {
        String nome = "João";
        String cpf = "053.545.648-89";
        LocalDate dataDeNascimento = LocalDate.now();
        NivelDeAcesso nivelDeAcesso = NivelDeAcesso.ALUNO;

        Aluno aluno = new Aluno(nome, cpf, dataDeNascimento, nivelDeAcesso);

        Assertions.assertThat(aluno.getNome()).isEqualTo(nome);
        Assertions.assertThat(aluno.getCpf()).isEqualTo(cpf);
        Assertions.assertThat(aluno.getDataDeNascimento()).isEqualTo(dataDeNascimento);
        Assertions.assertThat(aluno.getNivelDeAcesso()).isEqualTo(nivelDeAcesso);
        Assertions.assertThat(aluno.getHistoricoDeNotas()).isEmpty();
    }

    @Test
    void deveAdicionarUmaNotaNoHistoricoDoAluno() {
        String nome = "João";
        String cpf = "053.545.648-89";
        LocalDate dataDeNascimento = LocalDate.now();
        NivelDeAcesso nivelDeAcesso = NivelDeAcesso.ALUNO;
        Aluno aluno = new Aluno(nome, cpf, dataDeNascimento, nivelDeAcesso);
        Integer nota = 9;
        Nota notaEsperada = new Nota (nota);

        aluno.adicionarNota(nota);

        Assertions.assertThat(aluno.getHistoricoDeNotas()).containsExactly(notaEsperada);
    }

    @Test
    void deveCalcularAMediaDoAluno() {
        String nome = "João";
        String cpf = "053.545.648-89";
        LocalDate dataDeNascimento = LocalDate.now();
        NivelDeAcesso nivelDeAcesso = NivelDeAcesso.ALUNO;
        Aluno aluno = new Aluno(nome, cpf, dataDeNascimento, nivelDeAcesso);
        Integer nota = 6;
        aluno.adicionarNota(nota);
        Integer outraNota = 10;
        aluno.adicionarNota(outraNota);
        Integer mediaEsperada = (nota + outraNota) / 2;

        Integer mediaDoAluno = aluno.obterMedia();

        Assertions.assertThat(mediaDoAluno).isEqualTo(mediaEsperada);
    }
}