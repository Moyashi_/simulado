package dominio.modelo.perfil;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import simulador.dominio.modelo.perfil.NivelDeAcesso;
import simulador.dominio.modelo.perfil.PerfilBase;

import java.time.LocalDate;

class PerfilBaseTest {

    @Test
    void deveCriarUmPerfilBase() {
        String nome = "joao";
        String cpf = "0533977168";
        LocalDate dataDeNasc = LocalDate.now();
        NivelDeAcesso nivelDeAcesso = NivelDeAcesso.ALUNO;

        PerfilBase perfilBase = new PerfilBase(nome, cpf, dataDeNasc, nivelDeAcesso);

        Assertions.assertThat(perfilBase.getNome()).isEqualTo(nome);
        Assertions.assertThat(perfilBase.getCpf()).isEqualTo(cpf);
        Assertions.assertThat(perfilBase.getDataDeNascimento()).isEqualTo(dataDeNasc);
        Assertions.assertThat(perfilBase.getNivelDeAcesso()).isEqualTo(nivelDeAcesso);
    }
}