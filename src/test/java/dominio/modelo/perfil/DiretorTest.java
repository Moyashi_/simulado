package dominio.modelo.perfil;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import simulador.dominio.modelo.perfil.NivelDeAcesso;
import simulador.dominio.modelo.perfil.diretor.Diretor;

import java.time.LocalDate;

class DiretorTest {

    @Test
    void deveCriarUmDiretor() {
        String nome = "João";
        String cpf = "053.545.648-89";
        LocalDate dataDeNascimento = LocalDate.now();
        NivelDeAcesso nivelDeAcesso = NivelDeAcesso.DIRETOR;

        Diretor diretor = new Diretor(nome, cpf, dataDeNascimento, nivelDeAcesso);

        Assertions.assertThat(diretor.getNome()).isEqualTo(nome);
        Assertions.assertThat(diretor.getCpf()).isEqualTo(cpf);
        Assertions.assertThat(diretor.getDataDeNascimento()).isEqualTo(dataDeNascimento);
        Assertions.assertThat(diretor.getNivelDeAcesso()).isEqualTo(nivelDeAcesso);
    }
}