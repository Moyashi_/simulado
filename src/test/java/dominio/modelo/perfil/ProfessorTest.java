package dominio.modelo.perfil;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import simulador.dominio.modelo.perfil.NivelDeAcesso;
import simulador.dominio.modelo.perfil.professor.Professor;

import java.time.LocalDate;

class ProfessorTest {

    @Test
    void deveCriarUmProfessor() {
        String nome = "João";
        String cpf = "053.545.648-89";
        LocalDate dataDeNascimento = LocalDate.now();
        NivelDeAcesso nivelDeAcesso = NivelDeAcesso.PROFESSOR;

        Professor professor = new Professor(nome, cpf, dataDeNascimento, nivelDeAcesso);

        Assertions.assertThat(professor.getNome()).isEqualTo(nome);
        Assertions.assertThat(professor.getCpf()).isEqualTo(cpf);
        Assertions.assertThat(professor.getDataDeNascimento()).isEqualTo(dataDeNascimento);
        Assertions.assertThat(professor.getNivelDeAcesso()).isEqualTo(nivelDeAcesso);
    }
}