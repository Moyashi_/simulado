package dominio.porta.entrada.comando.simulado;

import dominio.builder.ProvaBuilder;
import simulador.dominio.modelo.simulado.prova.Prova;
import simulador.dominio.porta.entrada.comando.simulado.RespondeQuestao;
import simulador.dominio.porta.entrada.comando.simulado.RespondeQuestaoConcreto;
import simulador.dominio.porta.entrada.comando.simulado.ResponderQuestao;
import simulador.dominio.porta.saida.ProvaRepository;
import simulador.dominio.modelo.simulado.questaodaprova.Questao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class RespondeQuestaoConcretoTest {

    private RespondeQuestao respondeQuestao;
    private ProvaRepository provaRepository;

    @BeforeEach
    public void setUp() {
        provaRepository = Mockito.mock(ProvaRepository.class);
        respondeQuestao = new RespondeQuestaoConcreto(provaRepository);
    }

    @Test
    void deveResponderQuestaoDaProva() throws Exception {
        Prova prova = new ProvaBuilder().criar();
        Questao questao = prova.getQuestoes().stream().findFirst().get();
        String resposta = "C";
        ResponderQuestao comando = new ResponderQuestao(prova.getId(), questao.getId(), resposta);
        Mockito.when(provaRepository.consultarProva(comando.getProvaId())).thenReturn(prova);
        Mockito.doNothing().when(provaRepository).atualizarProva(prova);

        respondeQuestao.executar(comando);

        Mockito.verify(provaRepository).consultarProva(prova.getId());
        Mockito.verify(provaRepository).atualizarProva(prova);
    }
}