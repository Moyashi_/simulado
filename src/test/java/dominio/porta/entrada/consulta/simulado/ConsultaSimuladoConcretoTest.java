package dominio.porta.entrada.consulta.simulado;

import dominio.builder.ProvaBuilder;
import simulador.dominio.modelo.simulado.prova.Prova;
import simulador.dominio.modelo.simulado.Simulado;
import simulador.dominio.porta.entrada.consulta.simulado.ConsultaSimulado;
import simulador.dominio.porta.entrada.consulta.simulado.ConsultaSimuladoConcreto;
import simulador.dominio.porta.entrada.consulta.simulado.SimuladoDTO;
import simulador.dominio.porta.saida.SimuladoRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class ConsultaSimuladoConcretoTest {

    private SimuladoRepository simuladoRepository;
    private ConsultaSimulado consultaSimulado;

    @BeforeEach
    public void setUp() {
        simuladoRepository = Mockito.mock(SimuladoRepository.class);
        consultaSimulado = new ConsultaSimuladoConcreto(simuladoRepository);
    }

    //TODO:Verificar uma maneira mais simples para comparar um Simulador e um SimuladorDTO.
    @Test
    void deveConsultarOsSimulados() {
        Prova prova = new ProvaBuilder().criar();
        Prova outraProva = new ProvaBuilder().criar();
        Simulado simulado = new Simulado(Collections.singletonList(prova));
        Simulado outorSimulado = new Simulado(Collections.singletonList(outraProva));
        List<Simulado> simulados = Arrays.asList(simulado, outorSimulado);
        Mockito.when(simuladoRepository.buscarSimulados()).thenReturn(simulados);

        List<SimuladoDTO> executar = consultaSimulado.executar();

        Assertions.assertThat(executar).hasSize(2);
    }
}