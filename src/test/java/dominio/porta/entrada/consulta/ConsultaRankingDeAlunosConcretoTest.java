package dominio.porta.entrada.consulta;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import simulador.dominio.modelo.perfil.NivelDeAcesso;
import simulador.dominio.modelo.perfil.aluno.Aluno;
import simulador.dominio.porta.entrada.consulta.aluno.AlunoDTO;
import simulador.dominio.porta.entrada.consulta.aluno.ConsultaRankingDeAlunos;
import simulador.dominio.porta.entrada.consulta.aluno.ConsultaRankingDeAlunosConcreto;
import simulador.dominio.porta.saida.AlunoRepository;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

class ConsultaRankingDeAlunosConcretoTest {

    private AlunoRepository alunoRepository;
    private ConsultaRankingDeAlunos consultaRankingDeAlunos;

    @BeforeEach
    public void setUp() {
        alunoRepository = Mockito.mock(AlunoRepository.class);
        consultaRankingDeAlunos = new ConsultaRankingDeAlunosConcreto(alunoRepository);
    }

    @Test
    void deveConsultarRankingDeAlunos() {
        Aluno aluno = criarAluno();
        Mockito.when(alunoRepository.consultarRanking()).thenReturn(Collections.singletonList(aluno));

        List<AlunoDTO> executar = consultaRankingDeAlunos.executar();

        Assertions.assertThat(executar)
                .extracting(alunoConsultado -> alunoConsultado.historicoDeNotas)
                .containsExactly(aluno.historicoDeNotas());
    }

    private Aluno criarAluno() {
        String nome = "João";
        String cpf = "053.545.648-89";
        LocalDate dataDeNascimento = LocalDate.now();
        NivelDeAcesso nivelDeAcesso = NivelDeAcesso.ALUNO;

        return new Aluno(nome, cpf, dataDeNascimento, nivelDeAcesso);
    }
}