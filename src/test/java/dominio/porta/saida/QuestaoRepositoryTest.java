package dominio.porta.saida;

import dominio.builder.QuestaoBuilder;
import simulador.dominio.modelo.simulado.questaodaprova.Questao;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.List;

@DataMongoTest
class QuestaoRepositoryTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    void deveSalvarUmaQuestao() {
        Questao questao = new QuestaoBuilder().criar();
        mongoTemplate.save(questao);

        List<Questao> all = mongoTemplate.findAll(Questao.class);

        Assertions.assertThat(all).isNotEmpty();
    }
}