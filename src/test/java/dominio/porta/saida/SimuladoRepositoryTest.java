package dominio.porta.saida;

import dominio.builder.ProvaBuilder;
import simulador.dominio.modelo.simulado.Simulado;
import simulador.dominio.modelo.simulado.prova.Prova;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import simulador.dominio.porta.saida.SimuladoRepository;

import java.util.Collections;
import java.util.List;

//@SpringBootTest(classes = SimuladorPdiApplication.class)
@DataMongoTest
class SimuladoRepositoryTest {

//    @Autowired
    private SimuladoRepository simuladoRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    void name() {
        Prova prova = new ProvaBuilder().criar();
        Simulado simulado = new Simulado(Collections.singletonList(prova));
        mongoTemplate.insert(simulado);

//        List<Simulado> simulados = simuladoRepository.buscarSimulados();
        List<Simulado> simulados = mongoTemplate.findAll(Simulado.class);

        Assertions.assertThat(simulados).containsExactly(simulado);
    }
}