package dominio.builder;

import simulador.dominio.modelo.simulado.questaodaprova.PesoDaQuestao;
import simulador.dominio.modelo.simulado.questaodaprova.Questao;
import simulador.dominio.modelo.simulado.questaodaprova.RespostaDaQuestao;

import java.util.Objects;

public class QuestaoBuilder {

    private String pergunta;
    private RespostaDaQuestao respostaDaQuestao;
    private RespostaDaQuestao respostaMarcada;
    private PesoDaQuestao pesoDaQuestao;

    public QuestaoBuilder() {
        this.pergunta = "qual a capital do brasil?";
        this.respostaDaQuestao = RespostaDaQuestao.B;
        this.pesoDaQuestao = PesoDaQuestao.FACIL;
    }

    public Questao criar() {
        Questao questao = new Questao(pergunta, respostaDaQuestao, pesoDaQuestao);
        if (Objects.nonNull(respostaMarcada)) {
            questao.responderQuestao(respostaMarcada);
        }
        return questao;
    }

    public QuestaoBuilder comRespostaMarcada(RespostaDaQuestao respostaMarcada) {
        this.respostaMarcada = respostaMarcada;
        return this;
    }

    public QuestaoBuilder respondidaCorretamente() {
        this.respostaMarcada = this.respostaDaQuestao;
        return this;
    }

    public QuestaoBuilder respondida() {
        this.respostaMarcada = RespostaDaQuestao.D;
        return this;
    }

    public QuestaoBuilder naoRespondida() {
        this.respostaMarcada = null;
        return this;
    }

    public QuestaoBuilder comPergunta(String pergunta) {
        this.pergunta = pergunta;
        return this;
    }

    public QuestaoBuilder comPesoDaQuestao(PesoDaQuestao pesoDaQuestao) {
        this.pesoDaQuestao = pesoDaQuestao;
        return this;
    }

    public QuestaoBuilder comRespostaDaQuestao(RespostaDaQuestao respostaDaQuestao) {
        this.respostaDaQuestao = respostaDaQuestao;
        return this;
    }
}
