package dominio.builder;

import simulador.dominio.modelo.simulado.prova.Prova;
import simulador.dominio.modelo.simulado.questaodaprova.PesoDaQuestao;
import simulador.dominio.modelo.simulado.questaodaprova.Questao;
import simulador.dominio.modelo.simulado.questaodaprova.RespostaDaQuestao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ProvaBuilder {

    private List<Questao> questoes;

    public ProvaBuilder() {
        this.questoes = montarQuestoesDaProva();
    }

    public Prova criar() {
        return new Prova(questoes);
    }

    public ProvaBuilder comQuestoes(List<Questao> questoes) {
        this.questoes = questoes;
        return this;
    }

    private List<Questao> montarQuestoesDaProva() {
        List<Questao> questoesFaceisDaProva = criarCincoQuestoes(PesoDaQuestao.FACIL);
        List<Questao> questaoMediaDaProva = criarCincoQuestoes(PesoDaQuestao.MEDIO);
        List<Questao> questaoDificilDaProva = criarCincoQuestoes(PesoDaQuestao.DIFICIL);

        return montarTodasQuestoesDaProva(questoesFaceisDaProva, questaoMediaDaProva, questaoDificilDaProva);
    }

    private List<Questao> criarCincoQuestoes(PesoDaQuestao pesoDaQuestao) {
        Questao questao = new QuestaoBuilder()
                .respondidaCorretamente()
                .comPesoDaQuestao(pesoDaQuestao)
                .criar();
        return Collections.nCopies(5, questao);
    }

    private ArrayList<Questao> montarTodasQuestoesDaProva(List<Questao> questoesFaceisDaProva,
                                                          List<Questao> questoesMediasDaProva,
                                                          List<Questao> questoesDificeisDaProva) {
        ArrayList<Questao> todasQuestoesDaProva = new ArrayList<>();
        todasQuestoesDaProva.addAll(questoesFaceisDaProva);
        todasQuestoesDaProva.addAll(questoesMediasDaProva);
        todasQuestoesDaProva.addAll(questoesDificeisDaProva);
        return todasQuestoesDaProva;
    }

    private List<RespostaDaQuestao> montarRespostasDaProva() {
        return this.questoes.stream().map(Questao::getRespostaDaQuestao).collect(Collectors.toList());
    }
}
